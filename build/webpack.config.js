const path = require('path')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

process.env.NODE_ENV = process.env.NODE_ENV || 'production'
module.exports = {
  mode: process.env.NODE_ENV,
  entry: path.resolve(__dirname, '../src/index.js'),
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, '../dist/'),
    libraryTarget: 'commonjs'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    alias: {
      urllib: path.resolve(__dirname, '../src/polyfill/urllib'),
      moment: path.resolve(__dirname, '../src/polyfill/moment'),
      request: path.resolve(__dirname, '../src/polyfill/request'),
      'iconv-lite': path.resolve(__dirname, '../src/polyfill/iconv-lite'),
      decamelize: path.resolve(__dirname, '../src/polyfill/decamelize')
    }
  },
  plugins: [
    // new BundleAnalyzerPlugin()
  ],
  devtool: false,
  target: 'node'
}
