import WxPayment from './weixin/payment/index'
import AliPayment from './alipay/payment/index'
import AppleIapPayment from './apple-iap/payment/index'
import WxV3Payment from './weixin-v3/payment/index'
import WxVirtualPayment from './weixin-virtual-payment/payment/index'
import HuaweiPayment from './huawei'

import { createApi } from './shared/index'

module.exports = {
  initWeixin: (options = {}) => {
    options.clientType = options.clientType || __ctx__.PLATFORM
    return createApi(WxPayment, options)
  },
  initAlipay: (options = {}) => {
    options.clientType = options.clientType || __ctx__.PLATFORM
    return createApi(AliPayment, options)
  },
  initAppleIapPayment: (options = {}) => {
    options.clientType = options.clientType || __ctx__.PLATFORM
    return createApi(AppleIapPayment, options)
  },
  initWeixinV3: (options = {}) => {
    options.clientType = options.clientType || __ctx__.PLATFORM
    return createApi(WxV3Payment, options)
  },
  initWeixinVirtualPayment: (options = {}) => {
    options.clientType = options.clientType || __ctx__.PLATFORM
    return createApi(WxVirtualPayment, options)
  },
  initHuawei: (options = {}) => {
    options.clientType = options.clientType || __ctx__.PLATFORM
    return createApi(HuaweiPayment, options)
  },
  HuaweiPaymentBizType: require('./huawei/biz-type')
}
