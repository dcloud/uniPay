const shouldDeleteCommonParams = ['resultCode', 'resultDesc', 'sign']
export default {
  getOrderInfo: {
    args: {
      _purify: {
        // subject在使用支付宝时必传，在微信支付这里特殊处理一下，直接删除
        shouldDelete: ['body', 'openid']
      },
      _post (params) {
        if (params.payer && !params.payer.userClientIp) {
          delete params.payer
        }

        return params
      },
      mercOrderNo: 'outTradeNo',
      tradeSummary: 'subject',
      totalAmount: 'totalFee',
      callbackUrl: 'notifyUrl',
      'payer.userClientIp': 'spbillCreateIp'
    }
  },
  orderQuery: {
    args: {
      sysTransOrderNo: 'transactionId',
      mercOrderNo: 'outTradeNo'
    },
    returnValue: {
      _purify: {
        shouldDelete: [...shouldDeleteCommonParams, 'orderStatus']
      },
      appId: 'appId',
      mchId: 'mercNo',
      outTradeNo: 'mercOrderNo',
      transactionId: 'sysTransOrderNo',
      totalFee: 'totalAmount',
      cashFee: 'payerAmount',
      tradeState: ({ orderStatus }) => {
        switch (orderStatus) {
          case 'TRX_SUCCESS': // 交易成功
            return 'SUCCESS'
          case 'TRX_FAILED': // 交易失败
            return 'FAIL'
          case 'TRX_APPLY': // 交易处理中
          case 'TRX_PROC': // 交易处理中
            return 'PROCESSING'
        }
      }
    }
  },
  refund: {
    args: {
      _purify: {
        shouldDelete: ['totalFee', 'refundFeeType']
      },
      sysTransOrderNo: 'transactionId',
      mercOrderNo: 'outTradeNo',
      mercRefundOrderNo: 'outRefundNo',
      refundAmount: 'refundFee',
      reason: 'refundDesc',
      callbackUrl: 'notifyUrl'
    },
    returnValue: {
      _purify: {
        shouldDelete: [...shouldDeleteCommonParams, 'payerRefundAmount']
      },
      outTradeNo: 'mercOrderNo',
      transactionId: 'sysTransOrderNo',
      outRefundNo: 'mercRefundOrderNo',
      refundId: 'sysRefundOrderNo',
      refundFee: 'refundAmount'
    }
  },
  refundQuery: {
    args: {
      _purify: {
        shouldDelete: ['outTradeNo', 'transactionId', 'offset']
      },
      mercRefundOrderNo: 'outRefundNo',
      sysRefundOrderNo: 'refundId'
    },
    returnValue: {
      _purify: {
        shouldDelete: [...shouldDeleteCommonParams, 'sysRefundOrderNo', 'mercRefundOrderNo', 'refundOrderStatus', 'finishTime', 'promotionRefundAmount', 'payerRefundAmount']
      },
      outTradeNo: 'mercOrderNo',
      transactionId: 'sysTransOrderNo',
      totalFee: 'totalFee',
      refundId: 'sysRefundOrderNo',
      refundFee: 'refundAmount',
      refundStatus: ({ refundOrderStatus }) => {
        switch (refundOrderStatus) {
          case 'REFUND_SUCCESS': // 退款成功
            return 'SUCCESS'
          case 'REFUND_FAILED': // 退款失败
            return 'FAIL'
          case 'REFUND_CHL_PROC': // 退款处理中
            return 'PROCESSING'
        }
      }
    }
  },
  verifyPaymentNotify: {
    returnValue: {
      _purify: {
        shouldDelete: [
          'payload',
          'promotionAmount',
          'paymentTools',
          'promotionDetail'
        ]
      },
      outTradeNo: 'mercOrderNo',
      transactionId: 'sysTransOrderNo',
      totalFee: 'totalAmount',
      cashFee: 'payerAmount',
      feeType: 'currency',
      timeEnd: 'finishTime',
      openid: 'payer.openId',
      returnCode: ({ orderStatus }) => {
        switch (orderStatus) {
          case 'TRX_SUCCESS': // 交易成功
            return 'SUCCESS'
          case 'TRX_FAILED': // 交易失败
            return 'FAIL'
        }
      }
    }
  },
  verifyRefundNotify: {
    returnValue: {
      _purify: {
        shouldDelete: [
          'promotionRefundAmount',
          'currency',
          'mercNo',
          'finishTime',
          'payload'
        ]
      },
      refundId: 'sysRefundNo',
      outRefundNo: 'mercRefundNo',
      refundFee: 'refundAmount',
      settlementRefundFee: 'payerRefundAmount',
      refundStatus: ({ refundOrderStatus }) => {
        switch (refundOrderStatus) {
          case 'REFUND_SUCCESS': // 退款成功
            return 'SUCCESS'
          case 'REFUND_FAILED': // 退款失败
            return 'FAIL'
          case 'REFUND_CHL_PROC': // 退款处理中
            return 'PROCESSING'
        }
      }
    }
  }
}
