export default class UniCloudError extends Error {
  constructor (options) {
    super(options.message)
    this.errMsg = options.message || ''
    this.errCode = options.code || ''
    Object.defineProperties(this, {
      message: {
        get () {
          return `errCode: ${this.errCode} | errMsg: ` + this.errMsg
        },
        set (msg) {
          this.errMsg = msg
        }
      }
    })
  }
}
