# protocols规则

不进行递归转化

```js
// 整体转化
apiName: {
    args: function(fromArgs) {
        fromArgs.a = fromArgs.a * 100
        return fromArgs
    }
}

// 参数转化
apiName: {
    args: {
        _pre (args){ // 应用其他转换规则之前处理
            return args
        },
        keyAfter:'keyBefore', // 直接转换属性名称的会删除旧属性名
        keyAfter_100: function(fromArgs) {
            // 通过function转化的如需删除旧属性名，需要注意防止其他转换的时候找不到
            return fromArgs.keyBefore * 100 - fromArgs.discount * 100
        },
        // 其他转换执行完毕之后统一处理一次参数，在_post之前执行
        _purify:{
            shouldDelete: ['keyBefore'], // 需删除的字段
        }
        _post (args){ // 应用其他转换规则之后处理
            return args
        },
    }
}
```