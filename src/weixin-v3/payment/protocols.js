export default {
  getOrderInfo: {
    args: {
      _purify: {
        // subject在使用支付宝时必传，在微信支付这里特殊处理一下，直接删除
        shouldDelete: ['subject']
      },
      'amount.total': 'totalFee',
      'payer.openid': 'openid',
      description: 'body',
      'sceneInfo.payerClientIp': 'spbillCreateIp'
    }
  },
  orderQuery: {
    returnValue: {
      _purify: {
        shouldDelete: ['appid', 'mchid', 'sceneInfo', 'promotionDetail']
      },
      totalFee: 'amount.total',
      cashFee: 'amount.payer_total'
    }
  },
  refund: {
    args: {
      'amount.total': 'totalFee',
      'amount.refund': 'refundFee',
      'amount.currency': 'refundFeeType',
      reason: 'refundDesc'
    },
    returnValue: {
      refundFee: 'amount.refund',
      cashRefundFee: 'amount.payer_refund'
    }
  },
  refundQuery: {
    args: {
      shouldDelete: ['outTradeNo', 'transactionId', 'refundId']
    },
    returnValue: {
      totalFee: 'amount.total',
      refundFee: 'amount.refund'
    }
  },
  downloadFundflow: {
    args: {
      accountType: (params) => {
        // 兼容V2，V3需要全部大写
        return params.accountType.toUpperCase()
      }
    }
  },
  verifyPaymentNotify: {
    returnValue: {
      totalFee: 'amount.total',
      cashFee: 'amount.payer_total',
      feeType: 'amount.currency',
      timeEnd: 'success_time',
      openid: 'payer.openid',
      returnCode: 'trade_state'
    }
  },
  verifyRefundNotify: {
    returnValue: {
      totalFee: 'amount.total',
      refundFee: 'amount.refund',
      settlementTotalFee: 'amount.payer_total',
      settlementRefundFee: 'amount.payer_refund',
      refundRecvAccout: 'user_received_account'
    }
  }
}
